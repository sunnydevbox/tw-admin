<?php

namespace Sunnydevbox\TWAdmin;

use Illuminate\Support\ServiceProvider;

class TWAdminServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {

    }   

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerCommands();
    }


    protected function registerProviders()
    {
        if (class_exists('\SleepingOwl\Admin\Providers\SleepingOwlServiceProvider')
            && !$this->app->resolved('\SleepingOwl\Admin\Providers\SleepingOwlServiceProvider')) {
            $this->app->register(\SleepingOwl\Admin\Providers\SleepingOwlServiceProvider::class);
        }

        if (class_exists('\Sunnydevbox\TWAdmin\Admin\Providers\AdminSectionsServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWAdmin\Admin\Providers\AdminSectionsServiceProvider')) {
            $this->app->register(\Sunnydevbox\TWAdmin\Admin\Providers\AdminSectionsServiceProvider::class);
        }

        if (class_exists('\Sunnydevbox\TWCore\TWCoreServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWCore\TWCoreServiceProvider')) {
            $this->app->register(\Sunnydevbox\TWCore\TWCoreServiceProvider::class);
        }

        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);
        }
    }


    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            /*if (! class_exists('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider') &&
                ! $this->app->resolved('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider')) {
                $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
            }*/

            $this->commands([
                //\Sunnydevbox\TWAdmin\Console\Commands\PublishMigrationsCommand::class,
                //\Sunnydevbox\TWAdmin\Console\Commands\PublishConfigCommand::class,
            ]);

            /*$localViewFactory = $this->createLocalViewFactory();
            $this->app->singleton(
                'command.sleepingowl.ide.generate',
                function ($app) use ($localViewFactory) {
                    return new \SleepingOwl\Admin\Console\Commands\GeneratorCommand($app['config'], $app['files'], $localViewFactory);
                }
            ); 

            $this->commands('command.sleepingowl.ide.generate');*/
        }
    }
}
