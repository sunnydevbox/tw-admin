<?php

namespace Sunnydevbox\TWAdmin\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishMigrationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twadmin:publish-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWAdmin migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
    }

    public function fire()
    {
        echo 'fire';
    }
}
